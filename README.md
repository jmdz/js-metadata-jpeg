# Función especifica para metadatos en JPEG

Esta es la función especifica que se menciona en el [readme de fix-js (loadMetadata)](https://gitlab.com/jmdz/fix-js/blob/master/README.md#image), por lo que en principio no hace falta ejecutar nada. Con solo llamar a `loadMetadata` desde la imagen ya se ejecutan los procesos de esta biblioteca.

Idealmente debería incluir los estándares EXIF, IPTC y XMP, pero por ahora solo esta el primero.

Además esta biblioteca crea un objeto con las especificaciones de los metadatos en el objeto del tipo (dentro de `HTMLImageElement.types`). Este objeto de especificaciones contiene todas las versiones de EXIF (aunque algunas cosas han debido suponerse ya que aun no conseguí todas las especificaciones).

Esta biblioteca surge de mis quebraderos de cabeza con [exif-js](https://github.com/exif-js/exif-js), de la que por cierto toma algunas líneas de código.

Para verlo  funcionando → [Datos EXIF](http://jmdz.com.ar/apps/datos-exif) y [gitlab/jmdz/my-web](https://gitlab.com/jmdz/my-web).

## Traducción al español

Si se incluye el archivo `js-metadata-jpeg-es.js` los textos estarán traducidos al español.

## Especificaciones

Las que encontré (y en las que me base) están incluidas en la carpeta `specs`.

## Imágenes de prueba

* Para las pruebas de `orientation` las de esa carpeta (creadas por mi).
* Para el resto de pruebas las de los repositorios:
	- [haraldk/TwelveMonkeys](https://github.com/haraldk/TwelveMonkeys/tree/master/imageio/imageio-jpeg/src/test/resources)
	- [ianare/exif-samples](https://github.com/ianare/exif-samples/tree/master/jpg)

## TODO

* Incluir los estándares IPTC y XMP.
* Conseguir las versiones 1.0, 1.1, 2.0, 2.21 y 2.21 unificada de la especificación EXIF.
* Añadir en la definición de cada campo el valor por defecto.
* Corregir los problemas de codificación de `UserComment` (Unicode, JIS y undefined).
