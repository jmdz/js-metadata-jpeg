HTMLImageElement.types.aliases['image/jpeg'].metatags._translate({

	_properties:{
		ImageWidth:'Columnas',
		ImageLength:'Filas',
		BitsPerSample:'Bits por componente',
		Compression:'Compresión',
		PhotometricInterpretation:'Composición de pixel',
		Orientation:'Orientación',
		SamplesPerPixel:'Componentes del pixel',
		PlanarConfiguration:'Arreglo de datos',
		YCbCrSubSampling:'Ratio de submuestreo de Y a C',
		YCbCrPositioning:'Y y C de posicionamiento',
		XResolution:'Resolución horizontal',
		YResolution:'Resolución vertical',
		ResolutionUnit:'Unidad de resolución',
		StripOffsets:'Ubicación de los datos',
		RowsPerStrip:'Filas por tira',
		StripByteCounts:'Bytes por tira',
		JPEGInterchangeFormat:'Desplazamiento al JPEG SOI',
		JPEGInterchangeFormatLength:'Bytes de datos JPEG',
		TransferFunction:'Función de transferencia',
		WhitePoint:'Punto de cromaticidad blanco',
		PrimaryChromaticities:'Cromaticidades primarias',
		YCbCrCoefficients:'Coeficientes de transformación de color',
		ReferenceBlackWhite:'Valores de referencia en blanco y negro',
		DateTime:'Fecha y hora de cambio',
		ImageDescription:'Descripción de la imagen',
		Make:'Marca de la cámara',
		Model:'Modelo de la cámara',
		Software:'Software',
		Artist:'Artista',
		Copyright:'Copyright',
		ExifVersion:'Versión de EXIF',
		FlashpixVersion:'Versión de Flashpix',
		ColorSpace:'Espacio de color',
		Gamma:'Gama',
		ComponentsConfiguration:'Significado de los componentes',
		CompressedBitsPerPixel:'Modo de compresión de imagen',
		PixelXDimension:'Ancho',
		PixelYDimension:'Altura',
		MakerNote:'Notas del fabricante',
		UserComment:'Comentario de usuario',
		RelatedSoundFile:'Archivo de sonido relacionado',
		DateTimeOriginal:'Fecha y hora original',
		DateTimeDigitized:'Fecha y hora de digitalización',
		OffsetTime:'Zona horaria de cambio',
		OffsetTimeOriginal:'Zona horaria original',
		OffsetTimeDigitized:'Zona horaria de digitalización',
		SubSecTime:'Subsegundos de cambio',
		SubSecTimeOriginal:'Subsegundos original',
		SubSecTimeDigitized:'Subsegundos de digitalización',
		Temperature:'Temperatura ambiental',
		Humidity:'Humedad relativa ambiental',
		Pressure:'Presión atmosférica ambiental',
		WaterDepth:'Profundidad del agua',
		Acceleration:'Aceleración',
		CameraElevationAngle:'Ángulo de elevación de la cámara',
		ImageUniqueID:'Identificador único de imagen',
		CameraOwnerName:'Dueño de la cámara',
		BodySerialNumber:'Número de serie de la cámara',
		LensSpecification:'Especificaciones del objetivo',
		LensMake:'Marca del objetivo',
		LensModel:'Modelo del objetivo',
		LensSerialNumber:'Número de serie del objetivo',
		ExposureTime:'Tiempo de exposición',
		FNumber:'Número F',
		ExposureProgram:'Programa de exposición',
		SpectralSensitivity:'Sensibilidad espectral',
		PhotographicSensitivity:'Sensibilidad fotográfica',
		OECF:'Factor de conversión optoeléctrico',
		SensitivityType:'Tipo de sensibilidad fotográfica',
		StandardOutputSensitivity:'Sensibilidad estándar de salida (SOS)',
		RecommendedExposureIndex:'Indice de exposición recomendada (REI)',
		ISOSpeed:'Velocidad ISO',
		ISOSpeedLatitudeyyy:'Latitud yyy de la velocidad ISO',
		ISOSpeedLatitudezzz:'Latitud zzz de la velocidad ISO',
		ShutterSpeedValue:'Velocidad de obturación (APEX)',
		ApertureValue:'Apertura (APEX)',
		BrightnessValue:'Brillo (APEX)',
		ExposureBiasValue:'Compensación de exposición (APEX)',
		MaxApertureValue:'Máxima apertura del objetivo (APEX)',
		SubjectDistance:'Distancia del sujeto',
		MeteringMode:'Modo de medición',
		LightSource:'Fuente de luz',
		Flash:'Flash',
		FocalLength:'Longitud focal del objetivo',
		SubjectArea:'Área del sujeto',
		FlashEnergy:'Potencia del flash',
		SpatialFrequencyResponse:'Respuesta de frecuencia espacial',
		FocalPlaneXResolution:'Resolución X plano focal',
		FocalPlaneYResolution:'Resolución Y plano focal',
		FocalPlaneResolutionUnit:'Unidad de resolución del plano focal',
		SubjectLocation:'Localización del sujeto',
		ExposureIndex:'Índice de exposición',
		SensingMethod:'Método de sensor',
		FileSource:'Fuente de archivo',
		SceneType:'Tipo de escena',
		CFAPattern:'Patrón de CFA',
		CustomRendered:'Procesamiento',
		ExposureMode:'Modo de exposición',
		WhiteBalance:'Balance de blancos',
		DigitalZoomRatio:'Zoom digital',
		FocalLengthIn35mmFilm:'Longitud focal (equivalente 35mm)',
		SceneCaptureType:'Tipo de captura de escena',
		GainControl:'Control de ganancia',
		Contrast:'Control de contraste',
		Saturation:'Control de saturación',
		Sharpness:'Control de nitidez',
		DeviceSettingDescription:'Configuración del dispositivo',
		SubjectDistanceRange:'Rango de distancia al objeto',
		GPSVersionID:'Versión de GPS',
		GPSLatitudeRef:'Hemisferio de la latitud',
		GPSLatitude:'Latitud',
		GPSLongitudeRef:'Hemisferio de la longitud',
		GPSLongitude:'Longitud',
		GPSAltitudeRef:'Referencia de altitud',
		GPSAltitude:'Altitud',
		GPSTimeStamp:'Hora GPS',
		GPSSatellites:'Satélites usados en la medición',
		GPSStatus:'Estado receptor GPS',
		GPSMeasureMode:'Modo de medición GPS',
		GPSDOP:'Precisión de la medida',
		GPSSpeedRef:'Unidad de velocidad',
		GPSSpeed:'Velocidad del receptor GPS',
		GPSTrackRef:'Tipo de dirección de movimiento',
		GPSTrack:'Dirección de movimiento',
		GPSImgDirectionRef:'Tipo de dirección de la imagen',
		GPSImgDirection:'Dirección de la imágen',
		GPSMapDatum:'Relevamiento geodésico utilizado',
		GPSDestLatitudeRef:'Hemisferio de la latitud del destino',
		GPSDestLatitude:'Latitud de destino',
		GPSDestLongitudeRef:'Hemisferio de la longitud del destino',
		GPSDestLongitude:'Longitud del destino',
		GPSDestBearingRef:'Tipo de dirección al destino',
		GPSDestBearing:'Dirección al destino',
		GPSDestDistanceRef:'Unidad de la distancia al destino',
		GPSDestDistance:'Distancia al destino',
		GPSProcessingMethod:'Método de procesamiento GPS',
		GPSAreaInformation:'Zona del GPS',
		GPSDateStamp:'Fecha GPS',
		GPSDifferential:'Diferencial de corrección GPS',
		GPSHPositioningError:'Error de posicionamiento horizontal',
		InteroperabilityIndex:'Índice de interoperabilidad',
		ISOSpeedRatings:'Índices de velocidad ISO',
		ExifIFDPointer:'Puntero de datos EXIF',
		GPSInfoIFDPointer:'Puntero de datos GPS',
		InteroperabilityIFDPointer:'Puntero de datos de interoperabilidad',
		thumbnail:'Miniatura',
	},

	Compression:{
		1:'sin comprimir',
		6:'compresión JPEG',
	},

	Orientation:function($o){
		if($o.constructor===Object){

			let t={
				'top'   :'arriba',
				'right' :'derecha',
				'bottom':'abajo',
				'left'  :'izquierda',
				'width' :'ancho',
				'height':'alto',
			};

			$o.row=$o.row.replace(t);
			$o.col=$o.col.replace(t);
			$o.x=$o.x.replace(t);
			$o.y=$o.y.replace(t);

		}
		return $o;
	},

	YCbCrPositioning:{
		1:'centrado',
		2:'co-ubicado',
	},

	ResolutionUnit:{
		2:'pulgadas',
		3:'centímetros',
	},

	ColorSpace:{
		1:'sRGB',
		0XFFFF:'No calibrado',
	},

	MeteringMode:{
		0:'Desconocido',
		1:'Promedio',
		2:'Promedio ponderada en el centro',
		3:'Punto',
		4:'Multipunto',
		5:'Matricial',
		6:'Parcial',
		255:'Otro'
	},

	LightSource:{
		0:'Desconocida',
		1:'Luz de día',
		2:'Fluorescente',
		3:'Incandescente',
		4:'Flash',
		9:'Soleado',
		10:'Nublado',
		11:'Sombra',
		12:'Luz de dia fluorescente (D 5700 – 7100K)',
		13:'Fluorescente blanco diurno (N 4600 – 5400K)',
		14:'Fluorescente blanco frío (W 3900 – 4500K)',
		15:'Blanco fluorescente (WW 3200 – 3700K)',
		16:'Fluorescente blanco cálido (L 2600 - 3250K)',
		17:'Luz estándar A',
		18:'Luz estándar B',
		19:'Luz estándar C',
		20:'D55',
		21:'D65',
		22:'D75',
		23:'D50',
		24:'Estudio de tungsteno ISO',
		255:'Otra',
	},

	Flash:{
		0x0000:'Flash no disparado.',
		0x0001:'Flash disparado.',
		0x0005:'Luz de retorno no detectada.',
		0x0007:'Luz de retorno detectada.',
		0x0009:'Flash disparado, modo de flash obligatorio.',
		0x000D:'Flash disparado, modo de flash obligatorio, luz de retorno no detectada.',
		0x000F:'Flash disparado, modo de flash obligatorio, luz de retorno detectada.',
		0x0010:'Flash no disparado, modo de flash obligatorio.',
		0x0018:'Flash no disparado, modo de flash automático.',
		0x0019:'Flash disparado, modo de flash automático.',
		0x001D:'Flash disparado, modo de flash automático, luz de retorno no detectada.',
		0x001F:'Flash disparado, modo de flash automático, luz de retorno detectada.',
		0x0020:'Sin función de flash.',
		0x0041:'Flash disparado, modo de reducción de ojos rojos.',
		0x0045:'Flash disparado, modo de reducción de ojos rojos, luz de retorno no detectada.',
		0x0047:'Flash disparado, modo de reducción de ojos rojos, luz de retorno detectada.',
		0x0049:'Flash disparado, modo de flash obligatorio, modo de reducción de ojos rojos.',
		0x004D:'Flash disparado, modo de flash obligatorio, modo de reducción de ojos rojos, luz de retorno no detectada.',
		0x004F:'Flash disparado, modo de flash obligatorio, modo de reducción de ojos rojos, luz de retorno detectada.',
		0x0059:'Flash disparado, modo de flash automático, modo de reducción de ojos rojos.',
		0x005D:'Flash disparado, modo de flash automático, luz de retorno no detectada, modo de reducción de ojos rojos.',
		0x005F:'Flash disparado, modo de flash automático, luz de retorno detectada, modo de reducción de ojos rojos.',
	},

	ExposureProgram:{
		0:'No definido',
		1:'Manual',
		2:'Normal',
		3:'Prioridad apertura',
		4:'Prioridad velocidad',
		5:'Creativo',
		6:'Acción',
		7:'Retrato',
		8:'Paisaje',
	},

	FocalPlaneResolutionUnit:{
		2:'pulgadas',
		3:'centímetros',
	},

	SensingMethod:{
		1:'No definido',
		2:'Sensor de área de color de un chip',
		3:'Sensor de área de color de dos chips',
		4:'Sensor de área de color de tres chips',
		5:'Sensor de área de color secuencial',
		7:'Sensor trilineal',
		8:'Sensor lineal de color secuencial',
	},

	FileSource:{
		0:'otro',
		1:'escáner de tipo transparente',
		2:'escáner de tipo reflejo',
		3:'cámara fotográfica digital',
	},

	SceneType:{
		1:'Fotografiada directamente',
	},

	CustomRendered:{
		0:'Proceso normal',
		1:'Proceso personalizado',
	},

	ExposureMode:{
		0:'Exposición automática',
		1:'Exposición manual',
		2:'Horquillado automático',
	},

	WhiteBalance:{
		0:'Balance de blancos automático',
		1:'Balance de blancos manual',
	},

	SceneCaptureType:{
		0:'Estándar',
		1:'Paisaje',
		2:'Retrato',
		3:'Escena nocturna',
	},

	GainControl:{
		0:'Ninguna',
		1:'Bajo aumento de ganancia',
		2:'Alto aumento de ganancia',
		3:'Baja disminución de ganancia',
		4:'Alta disminución de ganancia',
	},

	Contrast:{
		0:'Normal',
		1:'Suave',
		2:'Fuerte',
	},

	Saturation:{
		0:'Normal',
		1:'Baja saturación',
		2:'Alta saturación',
	},

	Sharpness:{
		0:'Normal',
		1:'Suave',
		2:'Fuerte',
	},

	SubjectDistanceRange:{
		0:'Desconocida',
		1:'Macro',
		2:'Vista cercana',
		3:'Vista lejana',
	},

	SensitivityType:{
		0:'Desconocida',
		1:'Sensibilidad de salida estándar (SOS)',
		2:'Índice de exposición recomendado (REI)',
		3:'Velocidad ISO',
		4:'Sensibilidad de salida estándar (SOS) and índice de exposición recomendado (REI)',
		5:'Sensibilidad de salida estándar (SOS) and velocidad ISO',
		6:'Índice de exposición recomendado (REI) and velocidad ISO',
		7:'Sensibilidad de salida estándar (SOS) and índice de exposición recomendado (REI) and velocidad ISO',
	},

	GPSLatitudeRef:{
		N:'Norte',
		S:'Sur',
	},

	GPSLongitudeRef:{
		E:'Este',
		W:'Oeste',
	},

	GPSAltitudeRef:{
		0:'Sobre el nivel del mar',
		1:'Bajo el nivel del mar',
	},

	GPSStatus:{
		A:'Medición en curso',
		V:'Medición interrumpida',
	},

	GPSMeasureMode:{
		2:'Medición bidimensional',
		3:'Medición tridimensional',
	},

	GPSSpeedRef:{
		K:'Kilómetros por hora',
		M:'Millas por hora',
		N:'Nudos',
	},

	GPSTrackRef:{
		T:'Dirección verdadera',
		M:'Dirección magnética',
	},

	GPSImgDirectionRef:{
		T:'Dirección verdadera',
		M:'Dirección magnética',
	},

	GPSDestLatitudeRef:{
		N:'Norte',
		S:'Sur',
	},

	GPSDestLongitudeRef:{
		E:'Este',
		W:'Oeste',
	},

	GPSDestBearingRef:{
		T:'Dirección verdadera',
		M:'Dirección magnética',
	},

	GPSDestDistanceRef:{
		K:'Kilómetros',
		M:'Millas',
		N:'Nudos',
	},

	GPSDifferential:{
		0:'Medición sin corrección diferencial.',
		1:'Corrección diferencial aplicada',
	},

	InteroperabilityIndex:{
		R98:'Especificación de archivo R98 de las Reglas de Interoperabilidad Exif recomendadas (ExifR98) o archivo básico DCF estipulado por la regla de diseño para el sistema de archivos de cámara.',
		THM:'Archivo de miniatura DCF estipulado por la regla de diseño para el sistema de archivos de cámara.',
		R03:'Archivo de opciones DCF estipulado por la regla de diseño para el sistema de archivos de cámara.',
	},

});
