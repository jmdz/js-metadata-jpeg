{/* hi aux */

let aux={

	OrienTransf:function($img,$r,$f){
		try{
			if($r){
				$img.rotate($r);
				if($f){
					setTimeout(()=>$img.flipH(),99);
				}
			}
			else if($f){
				$img.flipH($f);
			}
			return true;
		}
		catch(e){
			log(e);
			return false;
		}
	},

	DateTime:function($value){
		if($value){
			let d=new Date($value.replace(/\u0000+$/,'').replace(/(\d{4}):(\d{2}):(\d{2})/,'$1-$2-$3'));
			if(!isNaN(d.getTime())){
				return d;
			}
		}
	},

	LatLong:function($value){
		if($value){
			if(typeof($value)==='object'&&$value.constructor===Array&&$value.length>=2){
				return ''+$value[0]+'°'+$value[1]+'\''+(+$value[2]>0?($value[2]+'\'\''):'');
			}
			else{
				return $value;
			}
		}
	},

	Undef2Ascii:function($value){
		if(typeof($value)==='object'&&$value.constructor===Uint8Array){
			return $value.toStringAsCharCode();
		}
		/* Not standard, but usually used. */
		else if(typeof($value)==='string'){
			return $value;
		}
	},

	Undef2Num:function($value){
		return +$value;
	},

	DotConcat:function($value){
		if(typeof($value)==='object'&&$value.constructor===Array){
			return $value.join('.');
		}
		/* Not standard, but usually used. */
		else if(typeof($value)==='string'){
			return $value;
		}
	},

	Translate:function($translation){

		let mt=HTMLImageElement.types.aliases['image/jpeg'].metatags;

		const pc=function($b){
			for(let l=Object.keys($b),i=0,p,v;i<l.length;i++){

				p=l[i];
				v=$b[p];

				if(!isNaN(+p)&&v.key){
					if($translation._properties[v.key]){
						v.name=$translation._properties[v.key];
					}
				}

				if(isNaN(+p)&&v.constructor===Object){
					if($translation[p]){
						if($translation[p].constructor===Object){
							for(let k of Object.keys(v)){
								if($translation[p][k]){
									v[k]=$translation[p][k];
								}
							}
						}
						else if($translation[p].constructor===Function){
							for(let k of Object.keys(v)){
								v[k]=$translation[p](v[k]);
							}
						}
					}
				}

			}
		};

		let sp=Object.keys(mt).filter($v=>$v.slice(0,5)==='exif_').sort();
		sp.unshift('pointers','tiff_6_0');

		for(let i=0;i<sp.length;i++){
			if(sp[i]==='pointers'||sp[i]==='tiff_6_0'){
				pc(mt[sp[i]]);
			}
			else{
				pc(mt[sp[i]].main);
				pc(mt[sp[i]].GPS);
				pc(mt[sp[i]].Interoperability);
			}
		}

	}

};

HTMLImageElement.types.aliases['image/jpeg'].metatags={

	_translate:aux.Translate,

	map_exif:{
		_Index:0x9000,
		_Preprocess:aux.Undef2Ascii,
		_Last:null,
		_LastV:null,
		_Props:{},
	},

	pointers:{

		ExifIFD:0x8769,
		0x8769:{key:'ExifIFDPointer',name:'Exif IFD',count:[1],type:['LONG']},

		GPSInfoIFD:0x8825,
		0x8825:{key:'GPSInfoIFDPointer',name:'GPS IFD',count:[1],type:['LONG']},

		InteroperabilityIFD:0xA005,
		0xA005:{key:'InteroperabilityIFDPointer',name:'Interoperability IFD',count:[1],type:['LONG']},

	},

	//TODO: agregar el valor por defecto a la definición de las propiedades

	tiff_6_0:{

		/* A. Tags relating to image data structure */
		0x0100:{key:'ImageWidth',name:'Image width',count:[1],type:['SHORT','LONG']},
		0x0101:{key:'ImageLength',name:'Image height',count:[1],type:['SHORT','LONG']},
			/* Is the image height. */
		0x0102:{key:'BitsPerSample',name:'Number of bits per component Compression scheme',count:[3],type:['SHORT']},
		0x0103:{key:'Compression',name:'Compression scheme',count:[1],type:['SHORT']},
		Compression:{
			1:'uncompressed',
			6:'JPEG compression',
			otherReserved:true,
		},
		0x0106:{key:'PhotometricInterpretation',name:'Pixel composition',count:[1],type:['SHORT']},
		PhotometricInterpretation:{
			2:'RGB',
			6:'YCbCr',
			otherReserved:true,
		},
		0x0112:{key:'Orientation',name:'Orientation of image',count:[1],type:['SHORT']},
		Orientation:{
			1:{row:'top'   ,col:'left'  ,x:'width' ,y:'height',transform:$img=>aux.OrienTransf($img,0  ,false),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			2:{row:'top'   ,col:'right' ,x:'width' ,y:'height',transform:$img=>aux.OrienTransf($img,0  ,true ),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			3:{row:'bottom',col:'right' ,x:'width' ,y:'height',transform:$img=>aux.OrienTransf($img,180,false),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			4:{row:'bottom',col:'left'  ,x:'width' ,y:'height',transform:$img=>aux.OrienTransf($img,180,true ),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			5:{row:'left'  ,col:'top'   ,x:'height',y:'width' ,transform:$img=>aux.OrienTransf($img,270,true ),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			6:{row:'right' ,col:'top'   ,x:'height',y:'width' ,transform:$img=>aux.OrienTransf($img,270,false),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			7:{row:'right' ,col:'bottom',x:'height',y:'width' ,transform:$img=>aux.OrienTransf($img,90 ,true ),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			8:{row:'left'  ,col:'bottom',x:'height',y:'width' ,transform:$img=>aux.OrienTransf($img,90 ,false),toString:function(){return `${this.row}&#8209;${this.col}`;}},
			otherReserved:true,
		},
		0x0115:{key:'SamplesPerPixel',name:'Number of components',count:[1],type:['SHORT']},
		0x011C:{key:'PlanarConfiguration',name:'Image data arrangement',count:[1],type:['SHORT']},
		PlanarConfiguration:{
			1:'chunky',
			2:'planar',
			otherReserved:true,
		},
		0x0212:{key:'YCbCrSubSampling',name:'Subsampling ratio of Y to C',count:[2],type:['SHORT']},
		_PreprocessValue_YCbCrSubSampling:aux.DotConcat,
		YCbCrSubSampling:{
			'2.1':'YCbCr4:2:2',
			'2.2':'YCbCr4:2:0',
			otherReserved:true,
		},
		0x0213:{key:'YCbCrPositioning',name:'Y and C positioning',count:[1],type:['SHORT']},
		YCbCrPositioning:{
			1:'centered',
			2:'co-sited',
			otherReserved:true,
		},
		0x011A:{key:'XResolution',name:'Image resolution in width direction',count:[1],type:['RATIONAL']},
		0x011B:{key:'YResolution',name:'Image resolution in height direction',count:[1],type:['RATIONAL']},
		0x0128:{key:'ResolutionUnit',name:'Unit of X and Y resolution',count:[1],type:['SHORT']},
		ResolutionUnit:{
			2:'inches',
			3:'centimeters',
			otherReserved:true,
		},

		/* B. Tags relating to recording offset */
		0x0111:{key:'StripOffsets',name:'Image data location',count:['Calculated'],type:['SHORT','LONG']},
		0x0116:{key:'RowsPerStrip',name:'Number of rows per strip',count:[1],type:['SHORT','LONG']},
		0x0117:{key:'StripByteCounts',name:'Bytes per compressed strip',count:['Calculated'],type:['SHORT','LONG']},
		0x0201:{key:'JPEGInterchangeFormat',name:'Offset to JPEG SOI',count:[1],type:['LONG']},
		0x0202:{key:'JPEGInterchangeFormatLength',name:'Bytes of JPEG data',count:[1],type:['LONG']},

		/* C. Tags relating to image data characteristics */
		0x012D:{key:'TransferFunction',name:'Transfer function',count:[3*256],type:['SHORT']},
		0x013E:{key:'WhitePoint',name:'White point chromaticity',count:[2],type:['RATIONAL']},
		0x013F:{key:'PrimaryChromaticities',name:'Chromaticities of primaries',count:[6],type:['RATIONAL']},
		0x0211:{key:'YCbCrCoefficients',name:'Color space transformation matrix coefficients',count:[3],type:['RATIONAL']},
		0x0214:{key:'ReferenceBlackWhite',name:'Pair of black and white reference values',count:[6],type:['RATIONAL']},

		/* D. Other tags */
		0x0132:{key:'DateTime',name:'File change date and time',count:[20],type:['ASCII']},
		DateTime:aux.DateTime,
		0x010E:{key:'ImageDescription',name:'Image title',count:['Any'],type:['ASCII']},
		0x010F:{key:'Make',name:'Image input equipment',count:['Any'],type:['ASCII']},
		0x0110:{key:'Model',name:'manufacturer',count:['Any'],type:['ASCII']},
		0x0131:{key:'Software',name:'Image input equipment model',count:['Any'],type:['ASCII']},
		0x013B:{key:'Artist',name:'Software used',count:['Any'],type:['ASCII']},
		0x8298:{key:'Copyright',name:'Person who created the image Copyright holder',count:['Any'],type:['ASCII']},

	},

	exif_01_00:{//TODO: conseguir
		/*
		*	Established image data format definitions
		*	Set definitions for structure of attribute information (tags)
		*	Established basic tag definitions
		*/
		document:'',//TODO: conseguir
		publication:'1995-10',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0100':'1.00',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA000:{key:'FlashpixVersion',name:'Supported Flashpix version',count:[4],type:['UNDEFINED']},
			_PreprocessValue_FlashpixVersion:aux.Undef2Ascii,
			FlashpixVersion:{
				'0100':'1.0',
				otherReserved:true,
			},

			/* C. Tags Relating to Image Configuration */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA002:{key:'PixelXDimension',name:'Valid image width',count:[1],type:['SHORT','LONG']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA003:{key:'PixelYDimension',name:'Valid image height',count:[1],type:['SHORT','LONG']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9101:{key:'ComponentsConfiguration',name:'Meaning of each component',count:[4],type:['UNDEFINED']},
			ComponentsConfiguration:function($value){
				let values={
					0:'',
					1:'Y',
					2:'Cb',
					3:'Cr',
					4:'R',
					5:'G',
					6:'B',
					otherReserved:true,
				};
				if($value){
					return Array.from(
						$value
						,function($v){return values[$v];}
					).join('');
				}
				else{
					return values;
				}
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9102:{key:'CompressedBitsPerPixel',name:'Image compression mode',count:[1],type:['RATIONAL']},

			/* D. Tags Relating to User Information */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x927C:{key:'MakerNote',name:'Manufacturer notes',count:['Any'],type:['UNDEFINED']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9286:{key:'UserComment',name:'User comments',count:['Any'],type:['UNDEFINED']},
			UserComment:function($value){
				if($value){
					let i=$value.slice(0,8).toHexArray().join('');
					let v=$value.slice(8);
					let s;
					switch(i){
						case '554E49434F444500':{
							//TODO: no siempre son 16bits
							s=new String(
								v.toUint16Array($value.littleEndian).toStringAsCharCode().replace(/\u0000+$/,'')
							);
							s.charset='Unicode';
						}break;
						case '4153434949000000':{
							s=new String(
								v.toStringAsCharCode().replace(/\u0000+$/,'')
							);
							s.charset='ASCII';
						}break;
						case '4A49530000000000':{
							//TODO: esto esta mal
							s=new String(
								v.toStringAsCharCode().replace(/\u0000+$/,'')
							);
							s.charset='JIS';
						}break;
						case '0000000000000000':{
							//TODO: ni idea
							s=new String(
								v.toStringAsCharCode().replace(/\u0000+$/,'')
							);
							s.charset='Undefined';
						}break;
						default:{
							s=new String(
								v.toStringAsCharCode().replace(/\u0000+$/,'')
							);
							s.charset=i;
						}break;
					}
					return s;
				}
				else{
					return false;
				}
			},

			/* E. Tag Relating to Related File Information */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA004:{key:'RelatedSoundFile',name:'Related audio file',count:[13],type:['ASCII']},

			/* F. Tags Relating to Date and Time */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9003:{key:'DateTimeOriginal',name:'Date and time of original data generation',count:[20],type:['ASCII']},
			DateTimeOriginal:aux.DateTime,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9004:{key:'DateTimeDigitized',name:'Date and time of digital data generation',count:[20],type:['ASCII']},
			DateTimeDigitized:aux.DateTime,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9290:{key:'SubSecTime',name:'DateTime subseconds',count:['Any'],type:['ASCII']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9291:{key:'SubSecTimeOriginal',name:'DateTimeOriginal subseconds',count:['Any'],type:['ASCII']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9292:{key:'SubSecTimeDigitized',name:'DateTimeDigitized subseconds',count:['Any'],type:['ASCII']},

			/* G. Tags Relating to Picture-Taking Conditions */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x829A:{key:'ExposureTime',name:'Exposure time',count:[1],type:['RATIONAL']},
				/* Seconds */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9201:{key:'ShutterSpeedValue',name:'Shutter speed',count:[1],type:['SRATIONAL']},
				/*
				APEX setting: ShutterSpeedValue = -1 * log2( ExposureTime )
				ExposureTime   ShutterSpeedValue
					30             -5
					2             -1
					1              0
					1/2            1
					1/1000        10
				*/
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9202:{key:'ApertureValue',name:'Aperture',count:[1],type:['RATIONAL']},
				/*
				APEX setting: ApertureValue = 2 * log2( FNumber )
				FNumber   ApertureValue
					1         0
					1.4       1
					2         2
					2.8       3
					32        10
				*/
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9203:{key:'BrightnessValue',name:'Brightness',count:[1],type:['SRATIONAL']},
				/*
				APEX setting: BrightnessValue = log2( B/NK )
				B: luminance in cd/cm²
				N:
					APEX: constant that establishes the relationship between the ASA arithmetic film speed and the ASA speed value, is 2^(-7/4)
					EXIF: speed scaling constant, is 8/25
				K: reflected-light meter calibration constant
					ISO-2720-1974: 0.00125
					EXIF: 0.00107
				footlambert   B=cd/cm²      BrightnessValue
					0.25         0.00008566    -2.00
					0.50         0.00017131    -1.00
					1            0.00034263     0.00
					2            0.00068525     1.00
					4            0.00137050     2.00
					8            0.00274101     3.00
					16            0.00548201     4.00
					32            0.01096403     5.00
					in the example of this specification say: 15fL → Bv=4 and 30fL → Bv=5, but this is incorrect
				*/
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9204:{key:'ExposureBiasValue',name:'Exposure bias',count:[1],type:['SRATIONAL']},
				/* APEX setting: ExposureBiasValue = ApertureValue + ShutterSpeedValue = BrightnessValue + log2 ( ASA * N ) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9205:{key:'MaxApertureValue',name:'Maximum lens aperture',count:[1],type:['RATIONAL']},
				/* APEX setting, see ApertureValue */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9206:{key:'SubjectDistance',name:'Subject distance',count:[1],type:['RATIONAL']},
				/* Meters */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9207:{key:'MeteringMode',name:'Metering mode',count:[1],type:['SHORT']},
			MeteringMode:{
				0:'Unknown',
				1:'Average',
				2:'CenterWeightedAverage',
				3:'Spot',
				4:'MultiSpot',
				5:'Pattern',
				6:'Partial',
				otherReserved:true,
				255:'Other'
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9208:{key:'LightSource',name:'Light source',count:[1],type:['SHORT']},
			LightSource:{
				0:'Unknown',
				1:'Daylight',
				2:'Fluorescent',
				3:'Tungsten',
				10:'Cloudy weather',
				17:'Standard light A',
				18:'Standard light B',
				19:'Standard light C',
				20:'D55',
				21:'D65',
				22:'D75',
				otherReserved:true,
				255:'Other',
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x9209:{key:'Flash',name:'Flash',count:[1],type:['SHORT']},
			Flash:{
				/*
				bit0: indicating whether the flash fired.
					0b0: Flash did not fire.
					0b1: Flash fired.
				bit1,2: indicating the status of returned light.
					0b00: No strobe return detect ion function.
					0b01: reserved.
					0b10: Strobe return light not detected.
					0b11: Strobe return light detected.
				*/
				/* 0b 00000 00 0 */ 0x0000:'Flash did not fire.',
				/* 0b 00000 00 1 */ 0x0001:'Flash fired.',
				/* 0b 00000 10 1 */ 0x0005:'Strobe return light not detected.',
				/* 0b 00000 11 1 */ 0x0007:'Strobe return light detected.',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x920A:{key:'FocalLength',name:'Lens focal length',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x829D:{key:'FNumber',name:'F number',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x8822:{key:'ExposureProgram',name:'Exposure program',count:[1],type:['SHORT']},
			ExposureProgram:{
				0:'Not defined',
				1:'Manual',
				2:'Normal program',
				3:'Aperture priority',
				4:'Shutter priority',
				5:'Creative program',
					/* (biased toward depth of field) */
				6:'Action program',
					/* (biased toward fast shutter speed) */
				7:'Portrait mode',
					/* (for closeup photos with the background out of focus) */
				8:'Landscape mode',
					/* (for landscape photos with the background in focus) */
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x8824:{key:'SpectralSensitivity',name:'Spectral sensitivity',count:['Any'],type:['ASCII']},
				/* ASTM - Standard Practice for the Electronic Interoperability of Color and Appearance Data (ASTM-E1708-95) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x8827:{key:'ISOSpeedRatings',name:'ISO speed rating',count:['Any'],type:['SHORT']},
				/* ISO Speed and ISO Latitude (ISO-12232-1998) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x8828:{key:'OECF',name:'Optoelectric conversion factor',count:['Any'],type:['UNDEFINED']},
				/* ISO - Methods for measuring opto-electronic conversion functions (OECFs) (ISO-14524-????) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA20B:{key:'FlashEnergy',name:'Flash energy',count:[1],type:['RATIONAL']},
				/* Beam Candlepower Seconds (BCPS) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA20C:{key:'SpatialFrequencyResponse',name:'Spatial frequency response',count:['Any'],type:['UNDEFINED']},
				/* ISO - Resolution measurements (ISO-12233-????) */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA20E:{key:'FocalPlaneXResolution',name:'Focal plane X resolution',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA20F:{key:'FocalPlaneYResolution',name:'Focal plane Y resolution',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA210:{key:'FocalPlaneResolutionUnit',name:'Focal plane resolution unit',count:[1],type:['SHORT']},
			FocalPlaneResolutionUnit:{
				2:'inches',
				3:'centimeters',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA214:{key:'SubjectLocation',name:'Subject location',count:[2],type:['SHORT']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA215:{key:'ExposureIndex',name:'Exposure index',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA217:{key:'SensingMethod',name:'Sensing method',count:[1],type:['SHORT']},
			SensingMethod:{
				1:'Not defined',
				2:'One-chip color area sensor',
				3:'Two-chip color area sensor',
				4:'Three-chip color area sensor',
				5:'Color sequential area sensor',
				7:'Trilinear sensor',
				8:'Color sequential linear sensor',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA300:{key:'FileSource',name:'File source',count:[1],type:['UNDEFINED']},
			_PreprocessValue_FileSource:aux.Undef2Num,
			FileSource:{
				3:'DSC',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA301:{key:'SceneType',name:'Scene type',count:[1],type:['UNDEFINED']},
			_PreprocessValue_SceneType:aux.Undef2Num,
			SceneType:{
				1:'Directly photographed',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA302:{key:'CFAPattern',name:'CFA pattern',count:['Any'],type:['UNDEFINED']},
			CFAPattern:function($value){
				let values={
					0x0:'R',
					0x1:'G',
					0x2:'B',
					0x3:'C',
					0x4:'M',
					0x5:'Y',
					0x6:'W',
				};
				if($value){
					let cols=$value.slice(0,2).toUint16Array($value.littleEndian)[0]-1;
					let vals=$value.slice(4);
					let a=[];
					let t=[];
					for(let i=0,r=0,c=0;i<vals.length;i++){
						if(typeof(a[r])==='undefined'){a[r]=[];t[r]='';}
						a[r][c]=values[vals[i]];
						t[r]+=a[r][c].slice(0,1);
						if(c<cols){
							c++;
						}
						else{
							c=0;
							r++;
						}
					}
					t=t.join('\n');
					return {asArray:a,asText:t};
				}
				else{
					return values;
				}
			},

		},
		GPS:{},
		Interoperability:{},
	},

	exif_01_10:{//TODO: conseguir
		/*
		*	Added tags
		*	Added operating specifications
		*/
		document:'',//TODO: conseguir
		publication:'1997-05',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0110':'1.10',
				otherReserved:true,
			},

		},
		GPS:{},
		Interoperability:{},
	},

	exif_02_00:{//TODO: conseguir
		/*
		*	Added sRGB color space
		*	Added GPS
		*	Added compressed thumbnails and audio files
		*/
		document:'',//TODO: conseguir
		publication:'1997-11',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0200':'2.00',
				otherReserved:true,
			},

			/* B. Tag Relating to Image Data Characteristics */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0xA001:{key:'ColorSpace',name:'Color space information',count:[1],type:['SHORT']},
			ColorSpace:{
				1:'sRGB',
				0XFFFF:'Uncalibrated',
				otherReserved:true,
			},

		},
		GPS:{

			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0000:{key:'GPSVersionID',name:'GPS tag version',count:[4],type:['BYTE']},
			_PreprocessValue_GPSVersionID:aux.DotConcat,
			GPSVersionID:{
				'2.0.0.0':'2.0',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0001:{key:'GPSLatitudeRef',name:'North or South Latitude',count:[2],type:['ASCII']},
			GPSLatitudeRef:{
				N:'North latitude',
				S:'South latitude',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0002:{key:'GPSLatitude',name:'Latitude',count:[3],type:['RATIONAL']},
			GPSLatitude:aux.LatLong,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0003:{key:'GPSLongitudeRef',name:'East or West Longitude',count:[2],type:['ASCII']},
			GPSLongitudeRef:{
				E:'East longitude',
				W:'West longitude',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0004:{key:'GPSLongitude',name:'Longitude',count:[3],type:['RATIONAL']},
			GPSLongitude:aux.LatLong,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0005:{key:'GPSAltitudeRef',name:'Altitude reference',count:[1],type:['BYTE']},
			GPSAltitudeRef:{
				0:'Sea level',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0006:{key:'GPSAltitude',name:'Altitude',count:[1],type:['RATIONAL']},
				/* Meters */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0007:{key:'GPSTimeStamp',name:'GPS time (atomic clock)',count:[3],type:['RATIONAL']},
				/* Hours, minutes and seconds in UTC */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0008:{key:'GPSSatellites',name:'GPS satellites used for measurement',count:['Any'],type:['ASCII']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0009:{key:'GPSStatus',name:'GPS receiver status',count:[2],type:['ASCII']},
			GPSStatus:{
				A:'Measurement in progress',
				V:'Measurement Interoperability',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000A:{key:'GPSMeasureMode',name:'GPS measurement mode',count:[2],type:['ASCII']},
			GPSMeasureMode:{
				2:'2-dimensional measurement',
				3:'3-dimensional measurement',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000B:{key:'GPSDOP',name:'Measurement precision',count:[1],type:['RATIONAL']},
				/* Degree of precision, depending of GPSMeasureMode: 2d is HDOP and 3d is PDOP */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000C:{key:'GPSSpeedRef',name:'Speed unit',count:[2],type:['ASCII']},
			GPSSpeedRef:{
				K:'Kilometers per hour',
				M:'Miles per hour',
				N:'Knots',
					/* 1 knots = 1 nautical mile per hour = 1852 m/h = 1.852 km/h */
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000D:{key:'GPSSpeed',name:'Speed of GPS receiver',count:[1],type:['RATIONAL']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000E:{key:'GPSTrackRef',name:'Reference for direction of movement',count:[2],type:['ASCII']},
			GPSTrackRef:{
				T:'True direction',
				M:'Magnetic direction',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x000F:{key:'GPSTrack',name:'Direction of movement',count:[1],type:['RATIONAL']},
				/* range = [ 0.00 , 359.99 ] */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0010:{key:'GPSImgDirectionRef',name:'Reference for direction of image',count:[2],type:['ASCII']},
			GPSImgDirectionRef:{
				T:'True direction',
				M:'Magnetic direction',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0011:{key:'GPSImgDirection',name:'Direction of image',count:[1],type:['RATIONAL']},
				/* range = [ 0.00 , 359.99 ] */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0012:{key:'GPSMapDatum',name:'Geodetic survey data used',count:['Any'],type:['ASCII']},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0013:{key:'GPSDestLatitudeRef',name:'Reference for latitude of destination',count:[2],type:['ASCII']},
			GPSDestLatitudeRef:{
				N:'North latitude',
				S:'South latitude',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0014:{key:'GPSDestLatitude',name:'Latitude of destination',count:[3],type:['RATIONAL']},
			GPSDestLatitude:aux.LatLong,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0015:{key:'GPSDestLongitudeRef',name:'Reference for longitude of destination',count:[2],type:['ASCII']},
			GPSDestLongitudeRef:{
				E:'East longitude',
				W:'West longitude',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0016:{key:'GPSDestLongitude',name:'Longitude of destination',count:[3],type:['RATIONAL']},
			GPSDestLongitude:aux.LatLong,
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0017:{key:'GPSDestBearingRef',name:'Reference for bearing of destination',count:[2],type:['ASCII']},
			GPSDestBearingRef:{
				T:'True direction',
				M:'Magnetic direction',
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0018:{key:'GPSDestBearing',name:'Bearing of destination',count:[1],type:['RATIONAL']},
				/* range = [ 0.00 , 359.99 ] */
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x0019:{key:'GPSDestDistanceRef',name:'Reference for distance to destination',count:[2],type:['ASCII']},
			GPSDestDistanceRef:{
				K:'Kilometers',
				M:'Miles',
				N:'Knots',
					/* 1 knots = 1 nautical mile per hour = 1852 m/h = 1.852 km/h */
				otherReserved:true,
			},
			// Propiedad trasladada por inferencia desde exif_02_10.
			0x001A:{key:'GPSDestDistance',name:'Distance to destination',count:[1],type:['RATIONAL']},

		},
		Interoperability:{},
	},

	exif_02_10:{
		/*
		*	Added DCF interoperability tags
		*/
		document:'JEIDA-49-1998',
		publication:'1998-12',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0210':'2.10',
				otherReserved:true,
			},

		},
		GPS:{},
		Interoperability:{

			0x0001:{key:'InteroperabilityIndex',name:'Interoperability Identification',count:['Any'],type:['ASCII']},
			InteroperabilityIndex:{
				R98:'R98 file specification of Recommended Exif Interoperability Rules (ExifR98) or DCF basic file stipulated by Design rule for Camera File System.',
				THM:'DCF thumbnail file stipulated by Design rule for Camera File System.',
			},

		},
	},

	exif_02_20:{
		/*
		*	Applied ExifPrint
			-	Developed tags for improving print quality (Photo contrast,
				sharpness, etc.)
		*	Added tags pertaining to positioning and GPS
		*/
		document:'JEITA-CP-3451',
		publication:'2002-04',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0220':'2.20',
				otherReserved:true,
			},

			/* G. Tags Relating to Picture-Taking Conditions */
			0x9206:{key:'SubjectDistance',name:'Subject distance',count:[1],type:['RATIONAL']},
				/* Meters. Note: if the numerator of the recorded value is FFFFFFFF.H, Infinity shall be indicated; and if the numerator is 0, Distance unknown shall be indicated. */
				/* Change: The note. */
			0x9208:{key:'LightSource',name:'Light source',count:[1],type:['SHORT']},
				/* Change: New values. */
			LightSource:{
				0:'Unknown',
				1:'Daylight',
				2:'Fluorescent',
				3:'Tungsten',
				4:'Flash',
				9:'Fine weather',
				10:'Cloudy weather',
				11:'Shade',
				12:'Daylight fluorescent (D 5700 – 7100K)',
				13:'Day white fluorescent (N 4600 – 5400K)',
				14:'Cool white fluorescent (W 3900 – 4500K)',
				15:'White fluorescent (WW 3200 – 3700K)',
				17:'Standard light A',
				18:'Standard light B',
				19:'Standard light C',
				20:'D55',
				21:'D65',
				22:'D75',
				23:'D50',
				24:'ISO studio tungsten',
				otherReserved:true,
				255:'Other light source',
			},
			0x9209:{key:'Flash',name:'Flash',count:[1],type:['SHORT']},
				/* Change: New values. */
			Flash:{
				/*
				bit0: indicating whether the flash fired.
					0b0: Flash did not fire.
					0b1: Flash fired.
				bit1,2: indicating the status of returned light.
					0b00: No strobe return detect ion function.
					0b01: reserved.
					0b10: Strobe return light not detected.
					0b11: Strobe return light detected.
				bit3,4: indicating the flash mode.
					0b00: unknown.
					0b01: Compulsory flash firing.
					0b10: Compulsory flash suppression.
					0b11: Auto mode.
				bit5: indicating the flash function.
					0b0: Flash function present.
					0b1: No flash function.
				bit6: indicating the red-eye mode.
					0b0: No red-eye reduction mode or unknown.
					0b1: Red-eye reduction supported.
				*/
				/* 0b 0 0 0 00 00 0 */ 0x0000:'Flash did not fire.',
				/* 0b 0 0 0 00 00 1 */ 0x0001:'Flash fired.',
				/* 0b 0 0 0 00 10 1 */ 0x0005:'Strobe return light not detected.',
				/* 0b 0 0 0 00 11 1 */ 0x0007:'Strobe return light detected.',
				/* 0b 0 0 0 01 00 1 */ 0x0009:'Flash fired, compulsory flash mode.',
				/* 0b 0 0 0 01 10 1 */ 0x000D:'Flash fired, compulsory flash mode, return light not detected.',
				/* 0b 0 0 0 01 11 1 */ 0x000F:'Flash fired, compulsory flash mode, return light detected.',
				/* 0b 0 0 0 10 00 0 */ 0x0010:'Flash did not fire, compulsory flash mode.',
				/* 0b 0 0 0 11 00 0 */ 0x0018:'Flash did not fire, auto mode.',
				/* 0b 0 0 0 11 00 1 */ 0x0019:'Flash fired, auto mode.',
				/* 0b 0 0 0 11 10 1 */ 0x001D:'Flash fired, auto mode, return light not detected.',
				/* 0b 0 0 0 11 11 1 */ 0x001F:'Flash fired, auto mode, return light detected.',
				/* 0b 0 0 1 00 00 0 */ 0x0020:'No flash function.',
				/* 0b 0 1 0 00 00 1 */ 0x0041:'Flash fired, red-eye reduction mode.',
				/* 0b 0 1 0 00 10 1 */ 0x0045:'Flash fired, red-eye reduction mode, return light not detected.',
				/* 0b 0 1 0 00 11 1 */ 0x0047:'Flash fired, red-eye reduction mode, return light detected.',
				/* 0b 0 1 0 01 00 1 */ 0x0049:'Flash fired, compulsory flash mode, red-eye reduction mode.',
				/* 0b 0 1 0 01 10 1 */ 0x004D:'Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected.',
				/* 0b 0 1 0 01 11 1 */ 0x004F:'Flash fired, compulsory flash mode, red-eye reduction mode, return light detected.',
				/* 0b 0 1 0 11 00 1 */ 0x0059:'Flash fired, auto mode, red-eye reduction mode.',
				/* 0b 0 1 0 11 10 1 */ 0x005D:'Flash fired, auto mode, return light not detected, red-eye reduction mode.',
				/* 0b 0 1 0 11 11 1 */ 0x005F:'Flash fired, auto mode, return light detected, red-eye reduction mode.',
				otherReserved:true,
			},
			0x9214:{key:'SubjectArea',name:'Subject area',count:[2,3,4],type:['SHORT']},
				/* Change: New tag. */
			0xA401:{key:'CustomRendered',name:'Custom image processing',count:[1],type:['SHORT']},
				/* Change: New tag. */
			CustomRendered:{
				0:'Normal process',
				1:'Custom process',
				otherReserved:true,
			},
			0xA402:{key:'ExposureMode',name:'Exposure mode',count:[1],type:['SHORT']},
				/* Change: New tag. */
			ExposureMode:{
				0:'Auto exposure',
				1:'Manual exposure',
				2:'Auto bracket',
				otherReserved:true,
			},
			0xA403:{key:'WhiteBalance',name:'White balance',count:[1],type:['SHORT']},
				/* Change: New tag. */
			WhiteBalance:{
				0:'Auto white balance',
				1:'Manual white balance',
				otherReserved:true,
			},
			0xA404:{key:'DigitalZoomRatio',name:'Digital zoom ratio',count:[1],type:['RATIONAL']},
				/* Note: If the numerator of the recorded value is 0, this indicates that digital zoom was not used. */
				/* Change: New tag. */
			0xA405:{key:'FocalLengthIn35mmFilm',name:'Focal length in 35 mm film',count:[1],type:['SHORT']},
				/* Note: A value of 0 means the focal length is unknown. */
				/* Change: New tag. */
			0xA406:{key:'SceneCaptureType',name:'Scene capture type',count:[1],type:['SHORT']},
				/* Change: New tag. */
			SceneCaptureType:{
				0:'Standard',
				1:'Landscape',
				2:'Portrait',
				3:'Night scene',
				otherReserved:true,
			},
			0xA407:{key:'GainControl',name:'Gain control',count:[1],type:['RATIONAL']},
				/* Change: New tag. */
			GainControl:{
				0:'None',
				1:'Low gain up',
				2:'High gain up',
				3:'Low gain down',
				4:'High gain down',
				otherReserved:true,
			},
			0xA408:{key:'Contrast',name:'Contrast',count:[1],type:['SHORT']},
				/* Change: New tag. */
			Contrast:{
				0:'Normal',
				1:'Soft',
				2:'Hard',
				otherReserved:true,
			},
			0xA409:{key:'Saturation',name:'Saturation',count:[1],type:['SHORT']},
				/* Change: New tag. */
			Saturation:{
				0:'Normal',
				1:'Low saturation',
				2:'High saturation',
				otherReserved:true,
			},
			0xA40A:{key:'Sharpness',name:'Sharpness',count:[1],type:['SHORT']},
				/* Change: New tag. */
			Sharpness:{
				0:'Normal',
				1:'Soft',
				2:'Hard',
				otherReserved:true,
			},
			0xA40B:{key:'DeviceSettingDescription',name:'Device settings description',count:['Any'],type:['UNDEFINED']},
				/* Change: New tag. */
			0xA40C:{key:'SubjectDistanceRange',name:'Subject distance range',count:[1],type:['SHORT']},
				/* Change: New tag. */
			SubjectDistanceRange:{
				0:'Unknown',
				1:'Macro',
				2:'Close view',
				3:'Distant view',
				otherReserved:true,
			},

			/* H. Other tags */
			0xA420:{key:'ImageUniqueID',name:'Unique image ID',count:[33],type:['ASCII']},
				/* Change: New tag. */

		},
		GPS:{

			0x0000:{key:'GPSVersionID',name:'GPS tag version',count:[4],type:['BYTE']},
				/* Change: Values. */
			_PreprocessValue_GPSVersionID:aux.DotConcat,
			GPSVersionID:{
				'2.2.0.0':'2.2',
				otherReserved:true,
			},
			0x0005:{key:'GPSAltitudeRef',name:'Altitude reference',count:[1],type:['BYTE']},
				/* Change: New values. */
			GPSAltitudeRef:{
				0:'Sea level',
				1:'Sea level reference (negative value)',
				otherReserved:true,
			},
			0x001B:{key:'GPSProcessingMethod',name:'Name of GPS processing method',count:['Any'],type:['UNDEFINED']},
				/* Change: New tag. */
			0x001C:{key:'GPSAreaInformation',name:'Name of GPS area',count:['Any'],type:['UNDEFINED']},
				/* Change: New tag. */
			0x001D:{key:'GPSDateStamp',name:'GPS date',count:[11],type:['ASCII']},
				/* String formated as "YYYY:MM:DD" with the date of UTC */
				/* Change: New tag. */
			0x001E:{key:'GPSDifferential',name:'GPS differential correction',count:[1],type:['SHORT']},
				/* Change: New tag. */
			GPSDifferential:{
				0:'Measurement without differential correction',
				1:'Differential correction applied',
				otherReserved:true,
			},

		},
		Interoperability:{},
	},

	exif_02_21:{//TODO: conseguir
		/*
		*	Added and corrected of Exif 2.2 content in line with revision of DCF
			2.0
			-	Added notation of tags for Gamma, ColorSpace, etc., that
				correspond to optional color space
			-	Changed content of notation of flash tags and FileSource tags
		*	Added operational guidelines (flash tags, scene capture type, etc)
		*	Corrected notation pertaining to image data pixel composition and
			pixel sampling
		*	Corrected misprints and omissions throughout the text
		*/
		document:'',//TODO: conseguir
		publication:'2003-09',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0221':'2.21',
				otherReserved:true,
			},

			/* B. Tag Relating to Image Data Characteristics */
			// Propiedad trasladada por inferencia desde exif_02_30.
			0xA500:{key:'Gamma',name:'Gamma',count:[1],type:['RATIONAL']},
				/* Change: New tag. */

			/* G. Tags Relating to Picture-Taking Conditions */
			// Propiedad trasladada por inferencia desde exif_02_30.
			0xA300:{key:'FileSource',name:'File source',count:[1],type:['UNDEFINED']},
				/* Change: New values. */
			_PreprocessValue_FileSource:aux.Undef2Num,
			FileSource:{
				0:'others',
				1:'scanner of transparent type',
				2:'scanner of reflex type',
				3:'DSC',
				otherReserved:true,
			},

		},
		GPS:{},
		Interoperability:{

			// Propiedad trasladada por inferencia desde exif_02_30.
			0x0001:{key:'InteroperabilityIndex',name:'Interoperability Identification',count:['Any'],type:['ASCII']},
				/* Change: New values. */
			InteroperabilityIndex:{
				R98:'R98 file specification of Recommended Exif Interoperability Rules (ExifR98) or DCF basic file stipulated by Design rule for Camera File System.',
				THM:'DCF thumbnail file stipulated by Design rule for Camera File System.',
				R03:'DCF Option File stipulated by Design rule for Camera File System.',
			},

		},
	},

	exif_02_21_u:{//TODO: conseguir
		/*
		*	Merged added/changed portion of Exif 2.21 to Exif 2.2
		*	Corrected misprints and omissions throughout the text
		*	Added “Guidelines for Handling Exif/DCF” issued by CIPA
			(CIPA DCG-004-2009) as Annex G
		*	Added explication of 2.2 as Annex H
		*	Added explication of 2.21 as Annex I
		*/
		document:'CIPA-DC-008-2009  / JEITA-CP-3451A',
		publication:'2003-09',
		main:{},
		GPS:{},
		Interoperability:{},
	},

	exif_02_30:{
		/*
		*	Restructured the main standard text, guidelines, explications, etc.,
			of Exif Unified Version 2.21
		*	Added and revised tags (Sensitivity-related tags, GPS information,
			camera and lens information, items pertaining to sound files, and
			light-source color)
		*	Clarified specification levels and revised the scope of application
		*	Supplemented explanations and adjusted format for the entire text
		*/
		document:'CIPA-DC-008-2010 / JEITA-CP-3451B',
		publication:'2010-04',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0230':'2.30',
				otherReserved:true,
			},

			/* G. Tags Relating to Picture-Taking Conditions */
			0x8827:{key:'PhotographicSensitivity',name:'Photographic Sensitivity',count:['Any'],type:['SHORT']},
				/* Sensitivity of the camera (ISO-12232), one of: standard output sensitivity (SOS), recommended exposure index (REI), or ISO speed. Accordingly to tag SensitivityType. */
				/* Change: Tag renamed and redefined (before was "ISOSpeedRatings"). */
			0x8830:{key:'SensitivityType',name:'Sensitivity Type',count:[1],type:['SHORT']},
				/* Change: New tag. */
			SensitivityType:{
				0:'Unknown',
				1:'Standard output sensitivity (SOS)',
				2:'Recommended exposure index (REI)',
				3:'ISO speed',
				4:'Standard output sensitivity (SOS) and recommended exposure index (REI)',
				5:'Standard output sensitivity (SOS) and ISO speed',
				6:'Recommended exposure index (REI) and ISO speed',
				7:'Standard output sensitivity (SOS) and recommended exposure index (REI) and ISO speed',
				otherReserved:true,
			},
			0x8831:{key:'StandardOutputSensitivity',name:'Standard Output Sensitivity',count:[1],type:['LONG']},
				/* Change: New tag. */
			0x8832:{key:'RecommendedExposureIndex',name:'Recommended ExposureIndex',count:[1],type:['LONG']},
				/* Change: New tag. */
			0x8833:{key:'ISOSpeed',name:'ISO Speed',count:[1],type:['LONG']},
				/* Change: New tag. */
			0x8834:{key:'ISOSpeedLatitudeyyy',name:'ISO Speed Latitude yyy',count:[1],type:['LONG']},
				/* Change: New tag. */
			0x8835:{key:'ISOSpeedLatitudezzz',name:'ISO Speed Latitude zzz',count:[1],type:['LONG']},
				/* Change: New tag. */
			0x9208:{key:'LightSource',name:'Light source',count:[1],type:['SHORT']},
				/* Change: New values. */
			LightSource:{
				0:'Unknown',
				1:'Daylight',
				2:'Fluorescent',
				3:'Tungsten',
				4:'Flash',
				9:'Fine weather',
				10:'Cloudy weather',
				11:'Shade',
				12:'Daylight fluorescent (D 5700 – 7100K)',
				13:'Day white fluorescent (N 4600 – 5400K)',
				14:'Cool white fluorescent (W 3900 – 4500K)',
				15:'White fluorescent (WW 3200 – 3700K)',
				16:'Warm white fluorescent (L 2600 - 3250K)',
				17:'Standard light A',
				18:'Standard light B',
				19:'Standard light C',
				20:'D55',
				21:'D65',
				22:'D75',
				23:'D50',
				24:'ISO studio tungsten',
				otherReserved:true,
				255:'Other light source',
			},

			/* H. Other tags */
			0xA430:{key:'CameraOwnerName',name:'Camera Owner Name',count:['Any'],type:['ASCII']},
				/* Change: New tag. */
			0xA431:{key:'BodySerialNumber',name:'Body Serial Number',count:['Any'],type:['ASCII']},
				/* Change: New tag. */
			0xA432:{key:'LensSpecification',name:'Lens Specification',count:[4],type:['RATIONAL']},
				/* Values (in order):
				 * Minimum focal length (unit: mm).
				 * Maximum focal length (unit: mm).
				 * Minimum F number in the minimum focal length.
				 * Minimum F number in the maximum focal length.
				*/
				/* Change: New tag. */
			0xA433:{key:'LensMake',name:'Lens Make',count:['Any'],type:['ASCII']},
				/* Change: New tag. */
			0xA434:{key:'LensModel',name:'Lens Model',count:['Any'],type:['ASCII']},
				/* Change: New tag. */
			0xA435:{key:'LensSerialNumber',name:'Lens Serial Number',count:['Any'],type:['ASCII']},
				/* Change: New tag. */

		},
		GPS:{

			0x0000:{key:'GPSVersionID',name:'GPS tag version',count:[4],type:['BYTE']},
				/* Change: Values. */
			_PreprocessValue_GPSVersionID:aux.DotConcat,
			GPSVersionID:{
				'2.3.0.0':'2.3',
				otherReserved:true,
			},
			0x001F:{key:'GPSHPositioningError',name:'Horizontal positioning error',count:[1],type:['RATIONAL']},
				/* Change: New tag. */

		},
		Interoperability:{},
	},

	exif_02_30_r:{
		/*
		*	Corrected an explanation part of Orientation Tag
		*	Corrected an explanation part of GPS Status Tag
		*/
		document:'CIPA-DC-008-2012  / JEITA-CP-3451C',
		publication:'2012-12',
		main:{},
		GPS:{

			0x0009:{key:'GPSStatus',name:'GPS receiver status',count:[2],type:['ASCII']},
				/* Change: Values. */
			GPSStatus:{
				A:'Measurement in progress',
				V:'Measurement interrupted',
				otherReserved:true,
			},

		},
		Interoperability:{},
	},

	exif_02_31:{
		/*
		*	Added time difference to UTC(Universal Time Coordinated) as tags
			relating to Date and Time
			-	Added three time offset tags respectively corresponding to the }
				three existing tags (File change date and time, Date and time of
				original data generation, Date and time of digital data
				generation)
		*	Added tags relating to shooting situation (Temperature, Humidity,
			Pressure, WaterDepth, Acceleration, CameraElevationAngle)
		*	Corrected misprints and omissions throughout the text (Including the
			reflection of the contents of the corrigendum established in
			September 2014 to the main text)
		*/
		document:'CIPA-DC-008-2016  / JEITA-CP-3451D',
		publication:'2016-07',
		main:{

			/* A. Tags Relating to Version */
			0x9000:{key:'ExifVersion',name:'Exif version',count:[4],type:['UNDEFINED']},
				/* Change: Values. */
			_PreprocessValue_ExifVersion:aux.Undef2Ascii,
			ExifVersion:{
				'0231':'2.31',
				otherReserved:true,
			},

			/* F. Tags Relating to Date and Time */
			0x9010:{key:'OffsetTime',name:'Offset data of DateTime',count:[7],type:['ASCII']},
				/* Offset from UTC (including daylight saving time) in the format "±HH:MM". When is unknown is "___:__" ("_" is space). If is blank, it is treated as unknown. */
				/* Change: New tag. */
			0x9011:{key:'OffsetTimeOriginal',name:'Offset data of DateTimeOriginal',count:[7],type:['ASCII']},
				/* Change: New tag. */
			0x9012:{key:'OffsetTimeDigitized',name:'Offset data of DateTimeDigitized',count:[7],type:['ASCII']},
				/* Change: New tag. */

			/* G2. Tags Relating to shooting situation */
			0x9400:{key:'Temperature',name:'Temperature',count:[1],type:['SRATIONAL']},
				/* The unit is °C. If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */
			0x9401:{key:'Humidity',name:'Humidity',count:[1],type:['RATIONAL']},
				/* The unit is %. If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */
			0x9402:{key:'Pressure',name:'Pressure',count:[1],type:['RATIONAL']},
				/* The unit is hPa. If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */
			0x9403:{key:'WaterDepth',name:'Water Depth',count:[1],type:['SRATIONAL']},
				/* The unit is m. When the value is negative, the absolute value of it indicates the height (elevation) above the water level. If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */
			0x9404:{key:'Acceleration',name:'Acceleration',count:[1],type:['RATIONAL']},
				/* The unit is mGal (10⁻⁵ m/s²). If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */
			0x9405:{key:'CameraElevationAngle',name:'Camera elevation angle',count:[1],type:['SRATIONAL']},
				/* The unit is degree(°). The range of the value is from -180 to less than 180. If the denominator of the recorded value is FFFFFFFF.H, unknown shall be indicated. */
				/* Change: New tag. */

		},
		GPS:{},
		Interoperability:{},
	},

};

aux.mt=HTMLImageElement.types.aliases['image/jpeg'].metatags;

aux.exif=Object.keys(aux.mt).filter($v=>$v.slice(0,5)==='exif_').sort();

const props_aliases=function($b,$s=null){
	let m=HTMLImageElement.types.aliases['image/jpeg'].metatags;
	let b=$s===null?m[$b]:m[$b][$s];
	let t=m.map_exif._Props;
	for(let l=Object.keys(b),i=0,p,v;i<l.length;i++){
		p=l[i];
		v=b[p];
		if(!isNaN(+p)&&v.key){
			if(!isDefined(t[v.key])){
				t[v.key]={_Last:null};
			}
			t[v.key]._Last=$b;
			t[v.key][$b]=v;
		}
	}
};

props_aliases('pointers');
props_aliases('tiff_6_0');

for(let i=0,v,a;i<aux.exif.length;i++){
	v=aux.exif[i];
	if(i){
		a=aux.exif[i-1];
		aux.mt[v].main=Object.assign(
			{}
			,aux.mt[a].main
			,aux.mt[v].main
		);
		aux.mt[v].GPS=Object.assign(
			{}
			,aux.mt[a].GPS
			,aux.mt[v].GPS
		);
		aux.mt[v].Interoperability=Object.assign(
			{}
			,aux.mt[a].Interoperability
			,aux.mt[v].Interoperability
		);
	}
	props_aliases(v,'main');
	props_aliases(v,'GPS');
	props_aliases(v,'Interoperability');
	aux.mt.map_exif._Last=v.slice(5,7)+v.slice(8,10);
	aux.mt.map_exif._LastV=parseInt(v.slice(5,7),10)+'.'+v.slice(8,10);
	aux.mt.map_exif[aux.mt.map_exif._Last]=v;
	aux.mt.map_exif[aux.mt.map_exif._LastV]=v;
}

HTMLImageElement.types.aliases['image/jpeg'].loadMetadata=async function(
	$img
	,$callback=function($metadata,$isCache,$img/*=this*/){}
){

	let arrayBuffer=await (new Response($img.blob)).arrayBuffer();

	let dataView=new DataView(arrayBuffer);

	const readTags=function($dataView,$findOffset,$littleEndian,$startOffset,$values={}){
		for(
			let i=0,entryOffset,tag,type,count,valueOffset;
			i<$dataView.getUint16($startOffset,$littleEndian);
			i++
		){

			entryOffset=$startOffset+i*12+2;
			tag=$dataView.getUint16(entryOffset,$littleEndian);
			type=$dataView.getUint16(entryOffset+2,$littleEndian);
			count=$dataView.getUint32(entryOffset+4,$littleEndian);
			valueOffset=$dataView.getUint32(entryOffset+8,$littleEndian)+$findOffset;

			switch(type){
				case 1:{  /* BYTE:      An 8-bit unsigned integer. */
					if(count==1){
						$values[tag]=$dataView.getUint8(entryOffset+8);
					}
					else{
						let vals=[];
						for(let n=0;n<count;n++){
							vals[n]=$dataView.getUint8((count>4?valueOffset:(entryOffset+8))+n);
						}
						$values[tag]=vals;
					}
				}break;
				case 2:{  /* ASCII:     An 8-bit byte containing one 7-bit ASCII code. The final byte is terminated with NULL. */
					$values[tag]=
						count
						?$dataView.getAsciiString(
							(count>4?valueOffset:(entryOffset+8))
							,count-1
						)
						:''
					;
				}break;
				case 3:{  /* SHORT:     A 16-bit (2-byte) unsigned integer. */
					if(count==1){
						$values[tag]=$dataView.getUint16(entryOffset+8,$littleEndian);
					}
					else{
						let vals=[];
						for(let n=0;n<count;n++){
							vals[n]=$dataView.getUint16((count>2?valueOffset:(entryOffset+8))+2*n,$littleEndian);
						}
						$values[tag]=vals;
					}
				}break;
				case 4:{  /* LONG:      A 32-bit (4-byte) unsigned integer. */
					if(count==1){
						$values[tag]=$dataView.getUint32(entryOffset+8,$littleEndian);
					}
					else{
						let vals=[];
						for(let n=0;n<count;n++){
							vals[n]=$dataView.getUint32(valueOffset+4*n,$littleEndian);
						}
						$values[tag]=vals;
					}
				}break;
				case 5:{  /* RATIONAL:  Two LONGs. The first LONG is the numerator and the second LONG expresses the denominator. */
					if(count==1){
						$values[tag]=Number.createFractional($dataView.getUint32(valueOffset,$littleEndian),$dataView.getUint32(valueOffset+4,$littleEndian));
					}
					else{
						let vals=[];
						for(let n=0;n<count;n++){
							vals[n]=Number.createFractional($dataView.getUint32(valueOffset+8*n,$littleEndian),$dataView.getUint32(valueOffset+4+8*n,$littleEndian));
						}
						$values[tag]=vals;
					}
				}break;
				case 7:{  /* UNDEFINED: An 8-bit byte that can take any value depending on the field definition. */
					let vals=new Uint8Array(count);
					vals.littleEndian=$littleEndian;
					for(let n=0;n<count;n++){
						vals[n]=$dataView.getUint8((count>4?valueOffset:(entryOffset+8))+n);
					}
					$values[tag]=vals;
				}break;
				case 9:{  /* SLONG:     A 32-bit (4-byte) signed integer (2's complement notat ion). */
					if(count==1){
						$values[tag]=$dataView.getInt32(entryOffset+8,$littleEndian);
					}
					else{
						let vals=[];
						for(let n=0;n<count;n++){
							vals[n]=$dataView.getInt32(valueOffset+4*n,$littleEndian);
						}
						$values[tag]=vals;
					}
				}break;
				case 10:{ /* SRATIONAL: Two SLONGs. The first SLONG is the numerator and the second SLONG is the denominator. */
					if(count==1){
						$values[tag]=$dataView.getInt32(valueOffset,$littleEndian)/$dataView.getInt32(valueOffset+4,$littleEndian);
					}
					else{
						let vals=[];
						for (let n=0;n<count;n++) {
							vals[n]=$dataView.getInt32(valueOffset+8*n,$littleEndian)/$dataView.getInt32(valueOffset+4+8*n,$littleEndian);
						}
						$values[tag]=vals;
					}
				}break;
			}

		}
		return $values;
	};

	const applySet=function($target,$set){
		for(let e in $target){
			if($set[e]){
				if($set[$set[e].key]){
					if(typeof($set[$set[e].key])==='function'){
						$target[$set[e].key]=$set[$set[e].key]($target[e]);
					}
					else if(
						typeof($set['_PreprocessValue_'+$set[e].key])==='function'
					){
						$target[$set[e].key]=$set[$set[e].key][
							$set['_PreprocessValue_'+$set[e].key]($target[e])
						];
					}
					else{
						$target[$set[e].key]=$set[$set[e].key][$target[e]];
					}
					if(typeof($target[$set[e].key])==='undefined'){
						$target['_PrimitiveValue_'+$set[e].key]=$target[e];
					}
				}
				else if($set[$set[e].key]!==false){
					$target[$set[e].key]=$target[e];
				}
				delete($target[e]);
			}
		}
		return $target;
	};

	const readEXIF=function($target,$dataView,$findOffset,$littleEndian,$startOffset){
		let mt=HTMLImageElement.types.aliases['image/jpeg'].metatags;
		let ev=mt.map_exif[mt.map_exif._Last];
		readTags(
			$dataView
			,$findOffset
			,$littleEndian
			,$startOffset
			,$target
		);
		applySet($target,mt.tiff_6_0);
		if(+$target[mt.pointers.ExifIFD]){
			readTags(
				$dataView
				,$findOffset
				,$littleEndian
				,$findOffset
					+$target[mt.pointers.ExifIFD]
				,$target
			);
			if(
				$target[mt.map_exif._Index]
				&&
				mt.map_exif._Preprocess($target[mt.map_exif._Index])
				&&
				mt.map_exif[mt.map_exif._Preprocess($target[mt.map_exif._Index])]
			){
				ev=mt.map_exif[
					mt.map_exif._Preprocess(
						$target[mt.map_exif._Index]
					)
				];
			}
			applySet($target,mt[ev].main);
		}
		if(+$target[mt.pointers.GPSInfoIFD]){
			readTags(
				$dataView
				,$findOffset
				,$littleEndian
				,$findOffset
					+$target[mt.pointers.GPSInfoIFD]
				,$target
			);
			applySet($target,mt[ev].GPS);
		}
		if(+$target[mt.pointers.InteroperabilityIFD]){
			readTags(
				$dataView
				,$findOffset
				,$littleEndian
				,$findOffset
					+$target[mt.pointers.InteroperabilityIFD]
				,$target
			);
			applySet($target,mt[ev].Interoperability);
		}
		applySet($target,mt.pointers);
	};

	let findOffset=2;
	while(findOffset<dataView.byteLength){
		if(dataView.getUint8(findOffset)!=0xFF){
			/* not a valid marker, something is wrong */
			break;
		}
		else if(dataView.getUint8(findOffset+1)==0xE1){/* 0xE1=21 */
			findOffset+=4;
			if(dataView.getUint32(findOffset)==0x45786966){/* 0x45786966='Exif' */
				findOffset+=6;
				let endian=dataView.getUint16(findOffset);
				let littleEndian=endian==0x4949;
				let firstIFDOffset=dataView.getUint32(findOffset+4,littleEndian);
				if(
					(
						littleEndian
						||
						endian==0x4D4D /* big */
					)
					&&
					dataView.getUint16(findOffset+2,littleEndian)==0x002A
					&&
					firstIFDOffset>=0x00000008
				){

					$img.metadata.exif={};

					readEXIF(
						$img.metadata.exif
						,dataView
						,findOffset
						,littleEndian
						,findOffset+firstIFDOffset
					);

					let IFD1OffsetPointer=dataView.getUint32(2+findOffset+firstIFDOffset+12*dataView.getUint16(findOffset+firstIFDOffset,littleEndian),littleEndian);
					if(
						0<IFD1OffsetPointer
						&&
						IFD1OffsetPointer<dataView.byteLength
					){

						$img.metadata.exif.thumbnail={};

						readEXIF(
							$img.metadata.exif.thumbnail
							,dataView
							,findOffset
							,littleEndian
							,findOffset+IFD1OffsetPointer
						);

						/* read thumbnail image */
						if(
							$img.metadata.exif.thumbnail.Compression===HTMLImageElement.types.aliases['image/jpeg'].metatags.tiff_6_0.Compression[6]
							&& $img.metadata.exif.thumbnail.JPEGInterchangeFormat
							&& $img.metadata.exif.thumbnail.JPEGInterchangeFormatLength
						){
							$img.metadata.exif.thumbnail.image=new Image();
							$img.metadata.exif.thumbnail.image.src='data:image/jpeg;base64,'+btoa(
								dataView.getAsciiString(
									findOffset+$img.metadata.exif.thumbnail.JPEGInterchangeFormat
									,$img.metadata.exif.thumbnail.JPEGInterchangeFormatLength
								)
							);
						}

					}

				}
			}
			break;
		}
		else{
			findOffset+=2+dataView.getUint16(findOffset+2);
		}
	}

//TODO:
// $img.metadata.iptc={};
// $img.metadata.xmp={};

	$callback.apply($img,[$img.metadata,false,$img]);

};

/* bye aux */}
